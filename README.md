# README #

This is for a job application at Royal London.

This is not intended as the final released version:

* Error handling is just through Exceptions.
* No attempt to scale for large files.
* Various bits hard-coded.
* No rounding of calculations as none specified.

## HOW TO RUN ##

* Run TestMethod1 under SystemTests/ProcessTest. Check the output in royallondontest/RoyalLondonTestApp/UnitTests/LiveData

## IMPROVEMENTS ##

* Combine DataReader and CsvReader by injecting the Extractor straight into CsvReader

